package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdicionarClienteAppObjects {

    private WebDriver driver;

    public AdicionarClienteAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement botaoAddCostumer(){
        return driver.findElement(By.xpath("//div[@class='floatL t5']//a"));
    }

    public WebElement name(){
        return driver.findElement(By.id("field-customerName"));
    }

    public WebElement lastName(){
        return driver.findElement(By.id("field-contactLastName"));
    }

    public WebElement contactFirstName(){
        return driver.findElement(By.id("field-contactFirstName"));
    }

    public WebElement phone(){
        return driver.findElement(By.id("field-phone"));

    }

    public WebElement addressLine1(){
        return driver.findElement(By.id("field-addressLine1"));
    }

    public WebElement addressLine2(){
        return driver.findElement(By.id("field-addressLine2"));

    }

    public WebElement city(){
        return driver.findElement(By.id("field-city"));

    }

    public WebElement state(){
        return driver.findElement(By.id("field-state"));

    }

    public WebElement postalCode(){
        return driver.findElement(By.id("field-postalCode"));

    }

    public  WebElement coutry(){
        return driver.findElement(By.id("field-country"));

    }

    public WebElement fromEmployeer(){
        return driver.findElement(By.xpath("//span[contains(text(),'Select from Employeer')]"));
    }

    public  WebElement bow(){
        return driver.findElement(By.xpath("//div[@class='chosen-search']//input"));
    }

    public WebElement clicarBow(){
        return driver.findElement(By.xpath("//div[@class='chosen-drop']//ul[@class='chosen-results']/li[@data-option-array-index='4']"));
    }

    public WebElement creditLimit(){
        return driver.findElement(By.id("field-creditLimit"));
    }

    public WebElement botaoSave(){
        return driver.findElement(By.id("save-and-go-back-button"));
    }

    public WebElement mensagemValidacao(){
        return driver.findElement(By.xpath("//span[contains(text(),'Your data has been successfully stored into the da')]"));
    }


}
