package AppObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BootstrapVersaoAppObjects {

    private WebDriver driver;

    public BootstrapVersaoAppObjects(WebDriver driver){
        this.driver = driver;
    }
    public WebElement selecionarV3(){
        return driver.findElement(By.id("switch-version-select"));
    }

    public WebElement selecionarV4(){
        return driver.findElement(By.xpath("//option[contains(text(),'Bootstrap V4 Theme')]"));
    }
}
