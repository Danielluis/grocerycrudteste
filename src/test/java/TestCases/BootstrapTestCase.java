package TestCases;

import Tasks.AdicionarClienteTasks;
import Tasks.BootstrapVersaoTasks;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

public class BootstrapTestCase {

    private WebDriver driver;
    private BootstrapVersaoTasks bootstrapVersaoTasks;
    private AdicionarClienteTasks adicionarClienteTasks;

    @BeforeEach
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
        this.driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");
        this.bootstrapVersaoTasks = new BootstrapVersaoTasks(driver);
        this.adicionarClienteTasks = new AdicionarClienteTasks(driver);
    }

    @AfterEach
    public void tearDown(){
//        driver.quit();
    }

    @Test
    public void Test(){
    bootstrapVersaoTasks.bootstrapVersao();
    adicionarClienteTasks.AdicionarCliente();


    }

}
