package Tasks;

import AppObjects.AdicionarClienteAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdicionarClienteTasks {

    private WebDriver driver;
    private AdicionarClienteAppObjects adicionarClienteAppObjects;

    public AdicionarClienteTasks(WebDriver driver){
        this.driver = driver;
        this.adicionarClienteAppObjects = new AdicionarClienteAppObjects(driver);
    }
    public void AdicionarCliente(){
        adicionarClienteAppObjects.botaoAddCostumer().click();
        adicionarClienteAppObjects.name().sendKeys("Daniel");
        adicionarClienteAppObjects.lastName().sendKeys("Siqueira");
        adicionarClienteAppObjects.contactFirstName().sendKeys("Daniel Siqueira");
        adicionarClienteAppObjects.phone().sendKeys("(51)1234-56789");
        adicionarClienteAppObjects.addressLine1().sendKeys("Rua vermelho, 123");
        adicionarClienteAppObjects.addressLine2().sendKeys("Rua Azul,123");
        adicionarClienteAppObjects.city().sendKeys("Orlando City");
        adicionarClienteAppObjects.state().sendKeys("Florida");
        adicionarClienteAppObjects.postalCode().sendKeys("32789");
        adicionarClienteAppObjects.coutry().sendKeys("EUA");
        adicionarClienteAppObjects.fromEmployeer().click();
        adicionarClienteAppObjects.bow().sendKeys("Bow");
        adicionarClienteAppObjects.clicarBow().click();
        adicionarClienteAppObjects.creditLimit().sendKeys("2.000.000,00");
        adicionarClienteAppObjects.botaoSave().click();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Your data has been successfully stored into the da')]")));
        String atual = adicionarClienteAppObjects.mensagemValidacao().getText();
        String expected = "Your data has been successfully stored into the database. Edit Customer    ";
        System.out.println(atual);
        Assertions.assertEquals(expected,atual );
    }

}
