package Tasks;

import AppObjects.BootstrapVersaoAppObjects;
import org.openqa.selenium.WebDriver;

public class BootstrapVersaoTasks {

    private WebDriver driver;
    private BootstrapVersaoAppObjects bootstrapVersaoAppObjects;

    public BootstrapVersaoTasks(WebDriver driver){
        this.driver = driver;
        this.bootstrapVersaoAppObjects = new BootstrapVersaoAppObjects(driver);
    }

    public void bootstrapVersao(){
        bootstrapVersaoAppObjects.selecionarV3().click();
        bootstrapVersaoAppObjects.selecionarV4().click();

    }
}
